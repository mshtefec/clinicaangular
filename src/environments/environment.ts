// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCY-xUEItlHV0Inn7Ch9HQuJI3C6rW67ZE",
    authDomain: "clinicaangular.firebaseapp.com",
    databaseURL: "https://clinicaangular.firebaseio.com",
    projectId: "clinicaangular",
    storageBucket: "clinicaangular.appspot.com",
    messagingSenderId: "1076385962850"
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
