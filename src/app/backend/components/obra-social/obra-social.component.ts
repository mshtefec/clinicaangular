import { Component, OnInit } from '@angular/core';
import { NgForm }            from '@angular/forms';
import { Router }            from '@angular/router';

//SERVICES
import { ObraSocialesService }     from '../../../backend/services/obra-sociales.service';
import { ObraSocial }            from '../../../backend/models/obraSocial'
@Component({
  selector: 'app-obra-social',
  templateUrl: './obra-social.component.html',
  styleUrls: ['./obra-social.component.css']
})
export class ObraSocialComponent implements OnInit {
  constructor(
    public router: Router,
    public obraSocialService: ObraSocialesService,
  
  ) { }
  ngOnInit() {
    this.obraSocialService.getObraSocial();
  }

  onSubmit(obraSocialFormulario: NgForm) {
    if(obraSocialFormulario.value.$key == null) {
      this.obraSocialService.insertarObraSocial(obraSocialFormulario.value);
    } else {
     // this.ObraSocialService.actualizarMedico(obraSocialFormulario.value);
    }    
    this.router.navigate(['admin/medicos']);
  }

}
