import { Component, OnInit, Input } from '@angular/core';
import { NgForm }            from '@angular/forms';
import { Router }            from '@angular/router';
//SERVICES
import { TurnosService }     from '../../../services/turnos.service';
import { Turno }            from '../../../models/turno';
import { MedicoService }            from '../../../services/medico.service';
//clases
import {Medico} from '../../../models/medico';
import { fork } from 'cluster';
@Component({
  selector: 'app-new-horario',
  templateUrl: './new-horario.component.html',
  styleUrls: ['./new-horario.component.css']
})
export class NewHorarioComponent implements OnInit {

 
  public medicos: Medico[];
  public list: Turno[];
  @Input() nombre: string;

  constructor(
    public router: Router,
    public turnosService: TurnosService,
    public medicoService:  MedicoService
  ) { }
  ngOnInit() {
    this.traerMedicos();
    this.turnosService.getTurno();
    this.resetForm();
    console.log("esto es lo me pasaron en el input" + this.nombre);
  }
  onSubmit(turnoFormulario: NgForm) {
   


    if(turnoFormulario.value.$key == null) {
      this.turnosService.insertarTurno(turnoFormulario.value);
    } else {
      //this.clientesService.actualizarPaciente(pacienteFormulario.value);
    }    
  
    this.resetForm(turnoFormulario);
    this.router.navigate(['admin/turno']);
  }

  resetForm(turnoFormulario?: NgForm) {
    if(turnoFormulario != null){
      turnoFormulario.reset();
      this.turnosService.selectedTurno = new Turno();
    }
  }

  traerMedicos() {
    this.medicoService.getMedicos()
    .snapshotChanges()
    .subscribe(
      item => {
        this.medicos = [];
        item.forEach(
          element => {
            let x = element.payload.toJSON();
            x['$key'] = element.key;
            this.medicos.push(x as Medico);
          }
        );
      }
    );
  }

  onRangoHorario(numbers : number){

    for (var _i = 0; _i < numbers; _i++) {
          
    }
    
  }

}
