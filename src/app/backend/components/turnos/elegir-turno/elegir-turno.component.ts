import { Component, OnInit } from '@angular/core';
import { NgForm }            from '@angular/forms';
import { Router }            from '@angular/router';
//SERVICES
import { TurnosService }     from '../../../services/turnos.service';
import { Turno }            from '../../../models/turno';
import { MedicoService }            from '../../../services/medico.service';
//clases
import {Medico} from '../../../models/medico';

@Component({
  selector: 'app-elegir-turno',
  templateUrl: './elegir-turno.component.html',
  styleUrls: ['./elegir-turno.component.css']
})
export class ElegirTurnoComponent implements OnInit {

  public medicos: Medico[];
  public list: Turno[];
  constructor(
    public router: Router,
    public turnosService: TurnosService,
    public medicoService:  MedicoService
  ) { }

  ngOnInit() {
    this.traerMedicos();
    this.turnosService.getTurno();
    this.resetForm();
  }
  onSubmit(turnoFormulario: NgForm) {
   


    if(turnoFormulario.value.$key == null) {
      this.turnosService.insertarTurno(turnoFormulario.value);
    } else {
      //this.clientesService.actualizarPaciente(pacienteFormulario.value);
    }    
  
    this.resetForm(turnoFormulario);
    this.router.navigate(['admin/turno']);
  }

  resetForm(turnoFormulario?: NgForm) {
    if(turnoFormulario != null){
      turnoFormulario.reset();
      this.turnosService.selectedTurno = new Turno();
    }
  }

  traerMedicos() {
    this.medicoService.getMedicos()
    .snapshotChanges()
    .subscribe(
      item => {
        this.medicos = [];
        item.forEach(
          element => {
            let x = element.payload.toJSON();
            x['$key'] = element.key;
            this.medicos.push(x as Medico);
          }
        );
      }
    );
  }

  onRangoHorario(numbers : number){

    for (var _i = 0; _i < numbers; _i++) {
          
    }
    
  }
}
