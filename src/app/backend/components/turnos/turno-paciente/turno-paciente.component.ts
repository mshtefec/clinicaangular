import { Component, OnInit } from '@angular/core';
import { NgForm }            from '@angular/forms';
import { Router }            from '@angular/router';
//SERVICES
import { TurnosService }     from '../../../../backend/services/turnos.service';
import { Turno }            from '../../../../backend/models/turno';
import { MedicoService }            from '../../../../backend/services/medico.service';
//clases
import {Medico} from '../../../models/medico';
import { fork } from 'cluster';

@Component({
  selector: 'app-turno-paciente',
  templateUrl: './turno-paciente.component.html',
  styleUrls: ['./turno-paciente.component.css']
})
export class TurnoPacienteComponent implements OnInit {

  public medicos: Medico[];
  public list: Turno[];
  public nombrePaciente: string;

  constructor(
    public router: Router,
    public turnosService: TurnosService,
    public medicoService:  MedicoService
  ) { }

  ngOnInit() {
    this.traerTodo();
    this.traerMedicos();
    this.turnosService.getTurno();
    this.resetForm();
  }
onSubmit(turnoFormulario: NgForm) {
   


    if(turnoFormulario.value.$key == null) {
      this.turnosService.insertarTurno(turnoFormulario.value);
    } else {
      //this.clientesService.actualizarPaciente(pacienteFormulario.value);
    }    
  
    this.resetForm(turnoFormulario);
    this.router.navigate(['admin/turno']);
  }

  resetForm(turnoFormulario?: NgForm) {
    if(turnoFormulario != null){
      turnoFormulario.reset();
      this.turnosService.selectedTurno = new Turno();
    }
  }

  onRangoHorario(numbers : number){

    for (var _i = 0; _i < numbers; _i++) {
          
    }
    
  }
  traerMedicos() {
    this.medicoService.getMedicos()
    .snapshotChanges()
    .subscribe(
      item => {
        this.medicos = [];
        item.forEach(
          element => {
            let x = element.payload.toJSON();
            x['$key'] = element.key;
            this.medicos.push(x as Medico);
          }
        );
      }
    );
  }
  traerTodo() {
    this.turnosService.getMedicoByDisponible(true)
    .snapshotChanges()
    .subscribe(
      item => {
        this.list = [];
        item.forEach(
          element => {
            let x = element.payload.toJSON();
            x['$key'] = element.key;
            this.list.push(x as Turno);
          }
        );
      }
    );
  }
  onBorrar(turno: string){
    console.log("este el el medico a borrar: " + turno);
    this.turnosService.borrarTurno(turno);
  }

  onTomarTurno(turno: Turno, $key: string){
    console.log("este es el input de paciente: " + this.nombrePaciente);
    this.turnosService.onTomarTurno(turno, $key, this.nombrePaciente);
  }
}
