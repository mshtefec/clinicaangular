import { Component, OnInit , ViewChild} from '@angular/core';
import { NgForm }            from '@angular/forms';
import { Router }            from '@angular/router';
import { NgModel }            from '@angular/forms';
//SERVICES
import { ToastrService } from 'ngx-toastr';
import { TurnosService }     from '../../../backend/services/turnos.service';
import { Turno }            from '../../../backend/models/turno';
import { MedicoService }            from '../../../backend/services/medico.service';
//clases
import {Medico} from '../../models/medico';
import { fork } from 'cluster';

 @Component({
  selector: 'app-turnos',
  templateUrl: './turnos.component.html',
  styleUrls: ['./turnos.component.css']
})
export class TurnosComponent implements OnInit {

  public medicos: Medico[];
  public list: Turno[];
  public editar: boolean = false;
  public nombrePaciente: string;
  public diaTurno: string;
  public horaTurno:string;
  public nombreMedico: string;
  public disponible: boolean;

  @ViewChild("informacion") informacion;
  constructor(
    public router: Router,
    public turnosService: TurnosService,
    public medicoService:  MedicoService,
    private toastrService: ToastrService
  ) 
  {}

  ngOnInit() {
    this.traerTodo();
    this.traerMedicos();
    this.turnosService.getTurno();
    this.resetForm();
    console.log(this.informacion.textContent);
  }
  onSubmit(turnoFormulario: NgForm) {
   


    if(turnoFormulario.value.$key == null) {
      this.turnosService.insertarTurno(turnoFormulario.value);
    } else {
      //this.clientesService.actualizarPaciente(pacienteFormulario.value);
    }    
  
    this.resetForm(turnoFormulario);
    this.router.navigate(['admin/turno']);
  }

  resetForm(turnoFormulario?: NgForm) {
    if(turnoFormulario != null){
      turnoFormulario.reset();
      this.turnosService.selectedTurno = new Turno();
    }
  }

  traerMedicos() {
    this.medicoService.getMedicos()
    .snapshotChanges()
    .subscribe(
      item => {
        this.medicos = [];
        item.forEach(
          element => {
            let x = element.payload.toJSON();
            x['$key'] = element.key;
            this.medicos.push(x as Medico);
          }
        );
      }
    );
  }

  onRangoHorario(numbers : number){

    for (var _i = 0; _i < numbers; _i++) {
          
    }
    
  }

  traerTodo() {
    this.turnosService.getTurno()
    .snapshotChanges()
    .subscribe(
      item => {
        this.list = [];
        item.forEach(
          element => {
            let x = element.payload.toJSON();
            x['$key'] = element.key;
            this.list.push(x as Turno);
          }
        );
      }
    );
  }
  onBorrar(turno: string){
    console.log("este el el medico a borrar: " + turno);
    this.turnosService.borrarTurno(turno);
  }

  onUno(turno: string){
    
    this.turnosService.getMedicoByID(turno)
    .snapshotChanges()
    .subscribe(
      item => {
        this.list = [];
        item.forEach(
          element => {
            let x = element.payload.toJSON();
            x['$key'] = element.key;
            this.list.push(x as Turno);
            console.log("onUno: " + this.list[0].paciente);
          }
        );
      }
    );
   
  }

   mostrar(){
    //  if (this.editar) {
    //   document.getElementById('idPaciente').style.display = 'block';
    //  }
    return this.editar;
    }

    onEditar($key: string){
      console.log("esto es lo que voy a editar: " + this.nombrePaciente + " " + this.diaTurno + " " + this.horaTurno + " " + this.nombreMedico +  " " + this.disponible);
      this.editar = true;
      this.list = null; 
        this.turnosService.editarTurno($key, this.nombrePaciente, this.diaTurno, this.horaTurno, this.nombreMedico, this.disponible);
    }
    onCancelarEditar(){
      this.turnosService.resetAlert();
      this.editar = false;
      this.nombrePaciente = null;
      this.nombreMedico = null;
      this.diaTurno = null;
      this.horaTurno = null;
      this.disponible = null;
     
    }
   
}
