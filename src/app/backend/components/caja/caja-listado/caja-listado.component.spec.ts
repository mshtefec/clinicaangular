import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CajaListadoComponent } from './caja-listado.component';

describe('CajaListadoComponent', () => {
  let component: CajaListadoComponent;
  let fixture: ComponentFixture<CajaListadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CajaListadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CajaListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
