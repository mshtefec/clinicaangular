import { Component, OnInit } from '@angular/core';
import { NgForm }            from '@angular/forms';
import { Router }            from '@angular/router';

//SERVICES
import { MedicoService }     from '../../../services/medico.service';

import { Medico }            from '../../../models/medico'
import { ObraSocialesService }     from '../../../services/obra-sociales.service';
import { ObraSocial }            from '../../../models/obrasocial'
@Component({
  selector: 'app-medico-formulario',
  templateUrl: './medico-formulario.component.html',
  styleUrls: ['./medico-formulario.component.css']
})
export class MedicoFormularioComponent implements OnInit {
  public obrasSociales: ObraSocial[];
  constructor(
    public router: Router,
    public medicoService: MedicoService,
    public obraSocialService: ObraSocialesService
  ) { }

  ngOnInit() {
    this.traerObrasSociales();
    this.medicoService.getMedicos();
    this.resetForm();
  }

  onSubmit(medicoFormulario: NgForm) {
    if(medicoFormulario.value.$key == null) {
      this.medicoService.insertarMedico(medicoFormulario.value);
    } else {
      this.medicoService.actualizarMedico(medicoFormulario.value);
    }    
    this.resetForm(medicoFormulario);
    this.router.navigate(['admin/medicos']);
  }

  resetForm(medicoFormulario?: NgForm) {
    if(medicoFormulario != null){
      medicoFormulario.reset();
      this.medicoService.selectedMedico = new Medico();
    }
  }
  traerObrasSociales() {
    this.obraSocialService.getObraSocial()
    .snapshotChanges()
    .subscribe(
      item => {
        this.obrasSociales = [];
        item.forEach(
          element => {
            let x = element.payload.toJSON();
            x['$key'] = element.key;
            this.obrasSociales.push(x as ObraSocial);
          }
        );
      }
    );
  }
}
