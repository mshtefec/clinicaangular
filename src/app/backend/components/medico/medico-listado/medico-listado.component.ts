import { Component, OnInit }      from '@angular/core';
import { MedicoService }          from '../../../services/medico.service';
import { Medico }                 from '../../../models/medico';

@Component({
  selector:     'app-medico-listado',
  templateUrl:  './medico-listado.component.html',
  styleUrls:    ['./medico-listado.component.css']
})
export class MedicoListadoComponent implements OnInit {

  public list: Medico[];
  public nombre: string;

  constructor(
    public medicoServ: MedicoService
  ) { }

  ngOnInit() {
    this.traerTodo();
  }

  onFiltrarPorEspecialidad(especialidad) {
    if (especialidad != "") {
      this.nombre = especialidad;  
      this.medicoServ.getMedicoByEspecialidad(this.nombre)
        .snapshotChanges()
        .subscribe(
          item => {
            this.list = [];
            item.forEach(
              element => {
                let x = element.payload.toJSON();
                x['$key'] = element.key;
                this.list.push(x as Medico);
              }
            );
          }
        );
    } else {
      this.traerTodo();
    }
  }

  traerTodo() {
    this.medicoServ.getMedicos()
    .snapshotChanges()
    .subscribe(
      item => {
        this.list = [];
        item.forEach(
          element => {
            let x = element.payload.toJSON();
            x['$key'] = element.key;
            this.list.push(x as Medico);
          }
        );
      }
    );
  }
  onBorrar(medico: string){
    console.log("este el el medico a borrar" + medico);
    this.medicoServ.borrarMedico(medico);
  }

}
