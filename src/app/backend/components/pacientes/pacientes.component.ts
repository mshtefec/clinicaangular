import { Component, OnInit } from '@angular/core';
import { ClientesService }          from '../../services/clientes.service';
import { Paciente }                 from '../../models/paciente';
@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit {

  public list: Paciente[];
  public nombre: string;
  public espera: boolean;
  constructor(
    public ClientesService: ClientesService,
  ) { }

  ngOnInit() {
    this.traerTodo();
  }
  onFiltrarPorObraSocial(Horario) {
    if (Horario != "") {
      this.nombre = Horario;  
      this.ClientesService.getClientesByObraSocial(this.nombre)
        .snapshotChanges()
        .subscribe(
          item => {
            this.list = [];
            item.forEach(
              element => {
                let x = element.payload.toJSON();
                x['$key'] = element.key;
                this.list.push(x as Paciente);
              }
            );
          }
        );
    } else {
      this.traerTodo();
    }
  }
  onFiltrarPorEspera(es) {
    if (es != "") {
      this.espera = es;  
      this.ClientesService.getClientesByespera(this.espera)
        .snapshotChanges()
        .subscribe(
          item => {
            this.list = [];
            item.forEach(
              element => {
                let x = element.payload.toJSON();
                x['$key'] = element.key;
                this.list.push(x as Paciente);
              }
            );
          }
        );
    } else {
      this.traerTodo();
    }
  }
  onFiltrarPorAtendidos(es) {
    if (es != true) {
      this.espera = es;  
      console.log("esasdasdasdasd:"+es);
      this.ClientesService.getClientesByAtendido(this.espera)
        .snapshotChanges()
        .subscribe(
          item => {
            this.list = [];
            item.forEach(
              element => {
                let x = element.payload.toJSON();
                x['$key'] = element.key;
                this.list.push(x as Paciente);
              }
            );
          }
        );
    } else {
      this.traerTodo();
    }
  }
  traerTodo() {
    this.ClientesService.getClientes()
    .snapshotChanges()
    .subscribe(
      item => {
        this.list = [];
        item.forEach(
          element => {
            let x = element.payload.toJSON();
            x['$key'] = element.key;
            this.list.push(x as Paciente);
          }
        );
      }
    );
  }
  onBorrar(es: string) {
    this.ClientesService.borrarPaciente(es);
  }
}
