import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPacientesComponent } from './new-pacientes.component';

describe('NewPacientesComponent', () => {
  let component: NewPacientesComponent;
  let fixture: ComponentFixture<NewPacientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPacientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPacientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
