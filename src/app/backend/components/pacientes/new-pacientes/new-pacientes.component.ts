import { Component, OnInit } from '@angular/core';
import { NgForm }            from '@angular/forms';
import { Router }            from '@angular/router';
//SERVICES
import { ClientesService }     from '../../../services/clientes.service';
import { ObraSocialesService }     from '../../../services/obra-sociales.service';

import { Paciente }            from '../../../models/paciente'
import { ObraSocial }            from '../../../models/obrasocial'


@Component({
  selector: 'app-new-pacientes',
  templateUrl: './new-pacientes.component.html',
  styleUrls: ['./new-pacientes.component.css']
})
export class NewPacientesComponent implements OnInit {

  public obrasSociales: ObraSocial[];

  constructor(
    public router: Router,
    public clientesService: ClientesService,
    public obraSocialService: ObraSocialesService
  ) { }

  ngOnInit() {
    this.traerObrasSociales();
    this.clientesService.getClientes();
    this.resetForm();
  }
  onSubmit(pacienteFormulario: NgForm) {
    if(pacienteFormulario.value.$key == null) {
      this.clientesService.insertarPaciente(pacienteFormulario.value);
    } else {
      this.clientesService.actualizarPaciente(pacienteFormulario.value);
    }    
    this.resetForm(pacienteFormulario);
    this.router.navigate(['admin/pacientes']);
  }

  resetForm(pacienteFormulario?: NgForm) {
    if(pacienteFormulario != null){
      pacienteFormulario.reset();
      this.clientesService.selectedPacientes = new Paciente();
    }
  }

  traerObrasSociales() {
    this.obraSocialService.getObraSocial()
    .snapshotChanges()
    .subscribe(
      item => {
        this.obrasSociales = [];
        item.forEach(
          element => {
            let x = element.payload.toJSON();
            x['$key'] = element.key;
            this.obrasSociales.push(x as ObraSocial);
          }
        );
      }
    );
  }
}
