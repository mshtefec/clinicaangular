import { Component, OnInit } from '@angular/core';

//SERVICES
import { MedicoService }      from './services/medico.service';
//MODEL
import { Medico }             from './models/medico'
//Login
import {LoginService} from './services/login.service';

@Component({
  selector: 'app-backend',
  templateUrl: './backend.component.html',
  styleUrls: ['./backend.component.css']
})
export class BackendComponent implements OnInit {

  list: Medico[];

  constructor(
    private medicoService: MedicoService,
    private loginService: LoginService,
   /* public isLogin: boolean,
    public nombreUsuario: string,
    public emailUsuario: string,
    public fotoUsuario: string
    */
  ) { }

  ngOnInit() {
    this.medicoService.getMedicos()
      .snapshotChanges()
      .subscribe(item => {
        this.list = [];
        item.forEach(element => {
          let x = element.payload.toJSON();
          x['$key'] = element.key;
          this.list.push(x as Medico);
        });
      });
      //login
      /*
      this.loginService.getAuth().subscribe( auth => {
        if (auth) {
          this.isLogin = true;
          this.nombreUsuario = auth.displayName;
          this.emailUsuario = auth.email; 
          this.fotoUsuario = auth.photoURL;
        } else {
          this.isLogin = false;
        }
      });*/
  }
  onCerrarSesion(){
   // this.loginService.logout();
  }
}
