import { HistoriaClinica } from "./historiaClinica";

export class Paciente {
    $key:               string
    nombre:             string
    apellido:           string
    dni:                number
    obraSocial:         string
    //obraSocial:         string[]
    historiaClinica:    string
    espera:             boolean
 

    constructor(nombre?: string, apellido?: string, dni?: number, obraSocial?: string, espera:boolean = false,historiaClinica?: string) {
        nombre          = this.nombre,
        apellido        = this.apellido,
        dni             = this.dni,
        obraSocial      = this.obraSocial,
        historiaClinica =historiaClinica,
        espera          = this.espera
    }
}
