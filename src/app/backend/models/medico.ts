export class Medico {
    $key:           string
    nombre:         string
    apellido:       string
    dni:            number
    especialidad:   string[]
    turno:   string
    obraSocial:         string
    //activo:     boolean

    constructor(nombre?: string, apellido?: string, dni?: number, especialidad?: string[], turno?: string,obraSocial?: string) {
        nombre          = this.nombre,
        apellido        = this.apellido,
        dni             = this.dni,
        especialidad    = this.especialidad,
        obraSocial      = this.obraSocial,
        turno        = this.turno
        //activo      = this.activo
    }
}
