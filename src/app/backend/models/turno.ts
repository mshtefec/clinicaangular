import { Time } from "../../../../node_modules/@angular/common";

export class Turno {
    $key:     string
    medico:   string
    paciente: string
    dniPaciente: string
    diaTurno: Date
    horaTurno: Time 
    disponible: boolean

    constructor(medico?: string, paciente?: string, diaTurno?: Date,horaTurno?: Time,disponible?: boolean, dniPaciente?: string) {
       medico = this.medico
       paciente = this.paciente
       dniPaciente = this.dniPaciente
       diaTurno = this.diaTurno
       horaTurno = this.horaTurno
       disponible = this.disponible
    }
}
