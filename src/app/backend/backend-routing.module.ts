import { NgModule }                     from '@angular/core';
import { Routes, RouterModule }         from '@angular/router';

import { BackendComponent }             from './backend.component';
import { Page404Component }             from '../api/page404/page404.component';
//MEDICO
import { MedicoListadoComponent }       from './components/medico/medico-listado/medico-listado.component';
import { MedicoFormularioComponent }    from './components/medico/medico-formulario/medico-formulario.component';
import { MedicoDetalleComponent }       from './components/medico/medico-detalle/medico-detalle.component';
//caja
import { CajaListadoComponent }         from './components/caja/caja-listado/caja-listado.component';
//Pacientes
import { PacientesComponent }           from './components/pacientes/pacientes.component';
import { NewPacientesComponent }        from './components/pacientes/new-pacientes/new-pacientes.component';
import { FacturaComponent }        from './components/factura/factura.component';
import { ObraSocialComponent } from './components/obra-social/obra-social.component';
import { TurnosComponent } from './components/turnos/turnos.component';
import {NewHorarioComponent} from './components/turnos/new-horario/new-horario.component'
import {TurnoPacienteComponent} from './components/turnos/turno-paciente/turno-paciente.component';
import {ElegirTurnoComponent} from './components/turnos/elegir-turno/elegir-turno.component';
const routes: Routes = [
  { 
    path: 'admin',
    component: BackendComponent,
    children: [
      { path: 'medicos', component: MedicoListadoComponent, pathMatch: 'full' },
      { path: 'medicos/nuevo', component: MedicoFormularioComponent, pathMatch: 'full' },
      { path: 'medicos/{key}', component: MedicoDetalleComponent, pathMatch: 'full' },
      { path: 'caja', component: CajaListadoComponent, pathMatch: 'full' },
      { path: 'pacientes', component: PacientesComponent, pathMatch: 'full' },
      { path: 'obraSocial', component: ObraSocialComponent, pathMatch: 'full' },
      { path: 'turno/turnoPaciente/elegirTurno', component: ElegirTurnoComponent, pathMatch: 'full' },
      { path: 'turno', component:  TurnosComponent, pathMatch: 'full' },
      { path: 'turno/newHorario', component:  NewHorarioComponent, pathMatch: 'full' },
      { path: 'turno/turnoPaciente', component:  TurnoPacienteComponent, pathMatch: 'full' },
      { path: 'pacientes/NewPacientes', component: NewPacientesComponent, pathMatch: 'full' },
      { path: 'pacientes/NewPacientes/Factura', component: FacturaComponent, pathMatch: 'full' },
      { path: '', redirectTo: 'medicos', pathMatch: 'full' },
      // { path: '**', component: Page404Component, pathMatch: 'full' },
    ]
  },
 //{ path: '', redirectTo: '/admin/medicos', pathMatch: 'full' },
  //{ path: '**', component: Page404Component, pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class BackendRoutingModule { }
