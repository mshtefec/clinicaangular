import { NgModule }                   from '@angular/core';
import { CommonModule }               from '@angular/common';
import { FormsModule }                from '@angular/forms';

//MODULOS
import { BackendRoutingModule }       from './backend-routing.module';

//COMPONENTES
import { MenuComponent }              from '../api/menu/menu.component';
import { BackendComponent }           from './backend.component';
//medicos
import { MedicoListadoComponent }     from './components/medico/medico-listado/medico-listado.component';
import { MedicoFormularioComponent }  from './components/medico/medico-formulario/medico-formulario.component';
import { MedicoDetalleComponent }     from './components/medico/medico-detalle/medico-detalle.component';
import { CajaListadoComponent } from './components/caja/caja-listado/caja-listado.component';
import { PacientesComponent } from './components/pacientes/pacientes.component';
import { NewPacientesComponent } from './components/pacientes/new-pacientes/new-pacientes.component';
import { FacturaComponent } from './components/factura/factura.component';
import { ObraSocialComponent } from './components/obra-social/obra-social.component';

import { TurnosComponent } from './components/turnos/turnos.component';
import { NewHorarioComponent } from './components/turnos/new-horario/new-horario.component';
import { TurnoPacienteComponent } from './components/turnos/turno-paciente/turno-paciente.component';
import { ElegirTurnoComponent } from './components/turnos/elegir-turno/elegir-turno.component';


@NgModule({
  imports: [
    CommonModule,
    BackendRoutingModule,
    FormsModule
  ],
  declarations: [
    MenuComponent,
    BackendComponent,
    MedicoListadoComponent,
    MedicoFormularioComponent,
    MedicoDetalleComponent,
    CajaListadoComponent,
    PacientesComponent,
    NewPacientesComponent,
    FacturaComponent,
    ObraSocialComponent,
    TurnosComponent,
    NewHorarioComponent,
    TurnoPacienteComponent,
    ElegirTurnoComponent,
  ]
})
export class BackendModule { }
