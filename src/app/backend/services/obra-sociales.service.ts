import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import {ObraSocial} from '../models/obraSocial';

@Injectable({
  providedIn: 'root'
})
export class ObraSocialesService {

  ObraSocialLista: AngularFireList<any>;
  selectedObraSocial: ObraSocial = new ObraSocial();

  constructor(
    private firebase: AngularFireDatabase
  ) { }

  getObraSocial(){
    return this.ObraSocialLista = this.firebase.list('ObraSocial');
  }

  

  insertarObraSocial(OS: ObraSocial){
    this.ObraSocialLista.push({
      nombre:    OS.nombre,
     // id:        OS.id
    })
  }

}
