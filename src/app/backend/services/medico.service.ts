import { Injectable } from '@angular/core';

import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import { Medico } from '../models/medico';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  medicosLista: AngularFireList<any>;
  selectedMedico: Medico = new Medico();

  constructor(
    private firebase: AngularFireDatabase
  ) { }

  getMedicos(){
    return this.medicosLista = this.firebase.list('medicos');
  }

  getMedicoByNombre(nombre: string){
    return this.medicosLista = this.firebase.list('medicos', ref => ref.orderByChild('nombre').equalTo(nombre));
  }

  getMedicoByEspecialidad(nombre: string){
    return this.medicosLista = this.firebase.list('medicos', ref => ref.orderByChild('especialidad').equalTo(nombre));
  }

  insertarMedico(medico: Medico){
    this.medicosLista.push({
      nombre:       medico.nombre,
      apellido:     medico.apellido,
      dni:          medico.dni,
      especialidad: medico.especialidad,
      obraSocial:   medico.obraSocial,
      turno: medico.turno
      //activo:   medico.activo
    })
  }

  actualizarMedico(medico: Medico){
    this.medicosLista.update(medico.$key, {
      nombre:       medico.nombre,
      apeliido:     medico.apellido,
      dni:          medico.dni,
      especialidad: medico.especialidad,
      obraSocial:   medico.obraSocial,
      turno: medico.turno
      //activo:   medico.activo
    })

  }

 borrarMedico($key: string){
   this.medicosLista.remove($key);
  }
  

}
