import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import *as firebase from 'firebase/app';
import { login } from '../models/login';
import { promise } from '../../../../node_modules/protractor';
import { resolve } from 'dns';
import { reject } from '../../../../node_modules/@types/q';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';

import { map } from "rxjs/operators";
//import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  loginLista: AngularFireList<any>;
  selectedLogin: login = new login();

  constructor(
    public firebase: AngularFireDatabase,
    public afAuth: AngularFireAuth
    
  ) { }
 

  registerUser(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, pass)
      .then( userData =>  resolve(userData),
      err => reject (err));
    });
  }
  
  loginEmail(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, pass)
      .then( userData =>  resolve(userData),
      err => reject (err));
    });
  }

  loginEm(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, pass)
      .then( userData =>  resolve(userData),
      err => reject (err));
    });
  }

  getAuth(){
    return this.afAuth.authState.pipe(map (auth => auth));
   // return this.afAuth.authState.map(auth => auth);
    
  }﻿

  logout() {
    return this.afAuth.auth.signOut();
  }
  
}
