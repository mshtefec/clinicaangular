import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import { Paciente } from '../models/paciente';

@Injectable({
  providedIn: 'root'
})
export class ClientesService { 
    PacientesLista: AngularFireList<any>;
    selectedPacientes: Paciente = new Paciente();
    temporal: string;
    constructor(
      private firebase: AngularFireDatabase,
     
    ) { }
  
    getClientes(){
      return this.PacientesLista = this.firebase.list('paciente');
    }
  
    getClientesByNombre(nombre: string){
      return this.PacientesLista = this.firebase.list('paciente', ref => ref.orderByChild('nombre').equalTo(nombre));
    }
  
    getClientesByObraSocial(nombre: string){
    
      return this.PacientesLista = this.firebase.list('paciente', ref => ref.orderByChild('obraSocial').equalTo(nombre));
    }
    getClientesByespera(espera: boolean){
      return this.PacientesLista = this.firebase.list('paciente', ref => ref.orderByChild('espera').equalTo(espera));
    }
    getClientesByAtendido(espera: boolean){
      return this.PacientesLista = this.firebase.list('paciente', ref => ref.orderByChild('espera').equalTo(espera));
    }
    insertarPaciente(paciente: Paciente){
      /*
      this.PacientesLista.push({
        nombre:           paciente.nombre,
        apellido:         paciente.apellido,
        dni:              paciente.dni,
        obraSocial:       paciente.obraSocial,
        espera:           true,
        //activo:   medico.activo
      })
      */
  
     this.temporal =  this.firebase.database.ref('paciente').push({
      nombre:           paciente.nombre,
      apellido:         paciente.apellido,
      dni:              paciente.dni,
      obraSocial:       paciente.obraSocial,
      historiaClinica:  paciente.historiaClinica,
      espera:           true,
    }).key;

    this.firebase.database.ref('paciente/'+this.temporal+'/ObraSocial').set({
    
      obraSocial:       paciente.obraSocial,
      
    })
    }

  
    actualizarPaciente(paciente: Paciente){
      this.PacientesLista.update(paciente.$key, {
        nombre:           paciente.nombre,
        apellido:         paciente.apellido,
        dni:              paciente.dni,
        obraSocial:       paciente.obraSocial,
        espera:           paciente.espera,
        //activo:   medico.activo

      })
  
    }
    insertarLogin(email?:string, password?: string, nombre?: string, apellido?: string, fechaNacimiento?: string, sexo?: string){
     console.log("Esto es sexo: " + sexo);
      if (sexo == "true") {
      this.PacientesLista.push({
        email:     email,
        password:     password,
        nombre:    nombre,
        apellido:    apellido,
        fechaNacimiento: fechaNacimiento,
        masculino: true,
      })
     } else {
      this.PacientesLista.push({
        email:     email,
        password:     password,
        nombre:    nombre,
        apellido:    apellido,
        fechaNacimiento: fechaNacimiento,
        femenino: true,
      })
     }
     
    }
    borrarPaciente($key: string){
      this.PacientesLista.remove($key);
    }
  }

