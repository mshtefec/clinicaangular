import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Turno } from '../models/turno';
import { Observable, Subject, throwError} from 'rxjs';
import { map } from 'rxjs/operators';
import { Time } from '../../../../node_modules/@angular/common';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class TurnosService {


  turnoLista: AngularFireList<any>;
   selectedTurno: Turno = new Turno();
   selectedTurnoDisponible: Turno[];
   cont:number = 0;
  constructor(
    private firebase: AngularFireDatabase,
    private toastrService: ToastrService
  ) { }

  getTurno(){
    return this.turnoLista = this.firebase.list('Turno');
  }
  getMedicoByDisponible(nombre: boolean){
    return this.turnoLista = this.firebase.list('Turno', ref => ref.orderByChild('disponible').equalTo(nombre));
  }
  getMedicoByID(nombre: string){
    return this.turnoLista = this.firebase.list('Turno', ref => ref.orderByKey().equalTo(nombre));
  }

  insertarTurno(turno: Turno){
    //for (var _i = 0; _i < 3; _i++) {
      if(turno.disponible == null){
        turno.disponible = false;
      }
 
      if (turno.paciente == null) {
        this.turnoLista.push({
          paciente: 'Disponible',
          medico: turno.medico,
          diaTurno: turno.diaTurno,
          horaTurno: turno.horaTurno,
          disponible: turno.disponible
       })
      }else{
        this.turnoLista.push({
          medico: turno.medico,
          paciente: turno.paciente,
          diaTurno: turno.diaTurno,
          horaTurno: turno.horaTurno,
          disponible: turno.disponible
       })
      }
  }
  
 borrarTurno($key: string){
  this.turnoLista.remove($key);
  
 }
 onTomarTurno(t: Turno, $key: string, nombrePaciente: string){
 

  var postData = {
    // Paciente: t.paciente,
    paciente: nombrePaciente,
    diaTurno: t.diaTurno,
    disponible: false,
    horaTurno: t.horaTurno,
    medico: t.medico
  };
  console.log("esto es lo me llega al onTOmarTUrno: " + postData.paciente + $key);
  this.turnoLista.update($key,postData);

 }
 editarTurno($key: string, nombrePaciente: string, diaTurno: string, horaTurno: string, nombreMedico: string, disponible: boolean){
  
  console.log("este ese el contando: " + this.cont);
  if(this.cont >= 1 ){
    
    this.cont ++ ;
  
    if (nombrePaciente == null || nombreMedico == null || diaTurno == null || horaTurno == null) {  
      
      this.toastrService.error('Error!', 'Los datos ingresados no son correctos');
      
      }
   else{
    var postData = {
      paciente: nombrePaciente,
      diaTurno: diaTurno,
      disponible: disponible,
      horaTurno: horaTurno,
      medico: nombreMedico
    };
    this.turnoLista.update($key,postData);
   }
  }
  this.cont ++ ;
}
resetAlert(){
  this.cont = 0;
}
}
