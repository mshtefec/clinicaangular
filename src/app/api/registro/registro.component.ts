import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel }            from '@angular/forms';
import { Router }            from '@angular/router';



//SERVICES
import { LoginService}     from '../../backend/services/login.service';
import {ClientesService} from '../../backend/services/clientes.service';
import { login }            from '../../backend/models/login';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  public email: string;
  public password: string;
  public nombre: string;
  public apellido: string;
  public fechaNacimiento: string;
  public customRadio: string;


  constructor(
    public LoginService: LoginService,
    private Router: Router,
    private clientesService: ClientesService
  ) { }

  ngOnInit() {
  }
 
  onSubmitAddUser() {
    console.log("esto es el primero: " + this.customRadio);
    this.LoginService.registerUser(this.email, this.password)
    .then((res) => {
      this.clientesService.getClientes();  
      this.clientesService.insertarLogin(this.email, this.password,this.nombre,this.apellido,this.fechaNacimiento,this.customRadio); 
      this.Router.navigate(['/admin'])
    console.log('Bien');
    console.log(res);
    }).catch( (err) => {
      console.log('mal');
      console.log(err);
    });
}
}
