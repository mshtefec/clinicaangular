import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel }            from '@angular/forms';
import { Router }            from '@angular/router';



//SERVICES
import { LoginService}     from '../../backend/services/login.service';

import { login }            from '../../backend/models/login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public email: string;
  public password: string;
  constructor(
    public LoginService: LoginService,
    public Router: Router,
   
  ) { }

  ngOnInit() {
    
  }
 
    onSubmitLogin() {
    this.LoginService.loginEmail(this.email, this.password)
    .then((res) => {
   
    this.Router.navigate(['/admin'])
    console.log('El login esta bien, esta bien el login');
    console.log(res);
    }).catch( (err) => {
      this.Router.navigate(['404'])
      console.log(err);
     
    });
}
}
