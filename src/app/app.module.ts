import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// MODULO DE RUTAS
import { RouterModule, Route } from '@angular/router';
import { BackendRoutingModule } from './backend/backend-routing.module';

import { BackendModule } from './backend/backend.module';

//FIREBASE
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {LoginService} from './backend/services/login.service';

// COMPONENTES
import { AppComponent } from './app.component';
 import { LoginComponent } from './api/login/login.component';
import { HomeComponent } from './frontend/home/home.component';
import {RegistroComponent} from './api/registro/registro.component';
// import { RegistroComponent } from './registro/registro.component';
import { Page404Component } from './api/page404/page404.component';
import { InformacionComponent } from './frontend/informacion/informacion.component';
import { login } from './backend/models/login';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';
import { LoginEmpresaComponent } from './api/login-empresa/login-empresa.component';
import { DescargaComponent } from './backend/components/descarga/descarga.component';

const rutas: Route[] = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'informacion', component:  InformacionComponent },
  { path: 'LoginEmpresa', component:  LoginEmpresaComponent },
  { path: 'descargar', component:  DescargaComponent },
  { path: '404', component:  Page404Component },
  { path: 'admin', redirectTo: '/admin' },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', redirectTo: '/home', pathMatch: 'full' },
  
]

@NgModule({
  declarations: [
    AppComponent,
     LoginComponent,
    HomeComponent,
   RegistroComponent,
    Page404Component,
    InformacionComponent,
    LoginEmpresaComponent,
    DescargaComponent,
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 5000,
    }),
    BackendModule,
    BackendRoutingModule,
    RouterModule.forRoot(rutas),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    FormsModule,
    HttpClientModule ,
    HttpModule,
    AngularFireAuthModule
    // Specify the library as an import
   
  ],
  providers: [LoginService],
  bootstrap: [AppComponent]
})



export class AppModule { }
